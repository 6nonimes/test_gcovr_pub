int	 foo(int param)
{
	if (param)
	{
		return 12;
	}
	else
	{
		return 0;
	}
}

int main(int argc, char* argv[])
{
	foo(0);
	return 0;
}
