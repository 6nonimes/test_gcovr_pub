/*
 * Exemple d'utilisation de Gcov par wikipedia
 * Sans erreur
 * */


#include <stdio.h>

int		test2(void)
{
  int i;

  for (i = 1; i <= 11; i++)
    {
      if (i % 3 == 0)
        printf ("%d is divisible by 3\n", i);
      if (i % 11 == 0)
        printf ("%d is divisible by 11\n", i);
    }

  return 0;
}
