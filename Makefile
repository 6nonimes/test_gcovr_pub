### gcovr use based on gcov exmaple of use by wikipedia
### gcc -Wall -fprofile-arcs -ftest-coverage cov.c -o a.exe
### example doc official :
#~  gcc -fprofile-arcs -ftest-coverage -fPIC -O0 exemple.c -o ex
### ./a.exe
### gcov cov.c

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
 GCOVR_CMD = gcovr
 FF_CMD = firefox
endif
ifeq ($(UNAME), Darwin)
 GCOVR_CMD = python gcovr
 FF_CMD = /Applications/Firefox.app/Contents/MacOS/firefox
endif
ifeq ($(UNAME), CYGWIN_NT-6.1)
 GCOVR_CMD = gcovr
 FF_CMD = "/cygdrive/c/Program Files (x86)/Mozilla Firefox/firefox.exe"
endif


NAME_SIMPLE = exemple_gcov_simple
SRC_SIMPLE =  exemple.c
SRC_NAME_SIMPLE = $(SRC:.c=)

NAME = exemple_gcov2
empty =
space = $(empty) $(empty)

HTML_FILES_ = $(subst $(space),$(space)$(NAME).,$(SRC))
HTML_FILES = $(HTML_FILES_:.c=.c.html)

SRC = $(space)exemple_gcov1.c exemple_gcov2.c exemple_gcov3.c main.c
SRC_NAME = $(SRC:.c=)
GCDA_FILES = $(SRC:.c=.gcda)
GCNO_FILES = $(SRC:.c=.gcno)

all: gcovr



gcovr:
	gcc -fprofile-arcs -ftest-coverage -fPIC -O0 $(SRC) -o $(NAME)
	./$(NAME)
	gcovr -r .
	gcovr -r . --html --html-details -o $(NAME).html

simple:
	gcc -fprofile-arcs -ftest-coverage -fPIC -O0 $(SRC_SIMPLE) -o $(NAME_SIMPLE)
	./$(NAME_SIMPLE)
	gcovr -r .
	gcovr -r . --html --html-details -o $(NAME_SIMPLE).html


ff:
	$(FF_CMD) $(NAME).html


clean:
	@rm -f $(GCDA_FILES)
	@rm -f $(GCNO_FILES)
	@rm -f $(NAME).html
	@rm -f $(HTML_FILES)

fclean: clean
	@rm -f $(NAME)

.PHONY: gcovr clean
